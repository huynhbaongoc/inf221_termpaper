\contentsline {section}{\numberline {1}Sorting Algorithms}{2}{section.1}
\contentsline {section}{\numberline {2}What happens inside each sorting algorithm?}{2}{section.2}
\contentsline {section}{\numberline {3}Pseudo Code and Python Implemetation}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Peliminary work}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Insertion Sort}{4}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Pseudo code}{4}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Python implementation}{4}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Test for Correctness}{4}{subsubsection.3.2.3}
\contentsline {subsection}{\numberline {3.3}Merge Sort}{5}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Pseudo code}{5}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Python Implementation}{5}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}Test for Correctness}{6}{subsubsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.4}Python Iterative Implementation}{6}{subsubsection.3.3.4}
\contentsline {subsection}{\numberline {3.4}Quick Sort}{7}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Pseudo code}{7}{subsubsection.3.4.1}
\contentsline {subsubsection}{\numberline {3.4.2}Python implementation}{8}{subsubsection.3.4.2}
\contentsline {subsubsection}{\numberline {3.4.3}Test for Correctness}{8}{subsubsection.3.4.3}
\contentsline {subsubsection}{\numberline {3.4.4}Python Iterative Implementation}{8}{subsubsection.3.4.4}
\contentsline {subsection}{\numberline {3.5}Heap sort}{9}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Pseudo code}{9}{subsubsection.3.5.1}
\contentsline {subsubsection}{\numberline {3.5.2}Python implementation}{9}{subsubsection.3.5.2}
\contentsline {subsubsection}{\numberline {3.5.3}Test for Correctness}{10}{subsubsection.3.5.3}
\contentsline {subsubsection}{\numberline {3.5.4}Python Iterative Implementation}{10}{subsubsection.3.5.4}
\contentsline {section}{\numberline {4}Benchmarking}{11}{section.4}
\contentsline {subsection}{\numberline {4.1}Generating test data}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Testing Environment}{13}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Perform benchmarking}{13}{subsection.4.3}
\contentsline {section}{\numberline {5}Performance}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Actual Performance Result vs Expected Result}{15}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Insertion Sort}{16}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Merge Sort}{17}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Quick Sort}{17}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}Heap Sort}{18}{subsubsection.5.1.4}
\contentsline {subsubsection}{\numberline {5.1.5}Actual Result matches Expected Result}{19}{subsubsection.5.1.5}
\contentsline {subsection}{\numberline {5.2}Performance comparison between sorting algorithms}{19}{subsection.5.2}
\contentsline {section}{\numberline {6}Discussion}{22}{section.6}
